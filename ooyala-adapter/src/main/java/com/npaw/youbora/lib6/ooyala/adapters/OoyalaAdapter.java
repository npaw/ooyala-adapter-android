package com.npaw.youbora.lib6.ooyala.adapters;

import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.adapter.PlayerAdapter;
import com.npaw.youbora.lib6.ooyala.BuildConfig;
import com.npaw.youbora.lib6.ooyala.adapters.ads.OoyalaAdsAdapter;

import com.ooyala.android.OoyalaException;
import com.ooyala.android.OoyalaNotification;
import com.ooyala.android.OoyalaPlayer;

import java.net.URL;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by Enrique on 14/02/2018.
 */

public class OoyalaAdapter extends PlayerAdapter<OoyalaPlayer> implements Observer {

    private Double lastPlayHead;

    private String lastErrorCode;
    private Long lastErrorSentTime;

    public OoyalaAdapter(OoyalaPlayer player) {
        super(player);
        lastPlayHead = 0.0;
        lastErrorSentTime = System.currentTimeMillis();
        registerListeners();
        //monitorPlayhead(false,true,400);
    }

    @Override
    public void registerListeners() {
        super.registerListeners();

        if (getPlayer() != null) getPlayer().addObserver(this);
        processPlayerState();
    }

    @Override
    public void unregisterListeners() {
        if (getPlayer() != null) getPlayer().deleteObserver(this);
        super.unregisterListeners();
    }

    @Override
    public void update(Observable observable, Object data) {
        OoyalaPlayer player = (OoyalaPlayer) observable;

        if (getPlayer() != player) return;

        if (getPlugin() != null && getPlugin().getAdsAdapter() == null) {
            YouboraLog.debug("Setting default ads adapter");
            getPlugin().setAdsAdapter(new OoyalaAdsAdapter(getPlayer()));
        }

        boolean isShowingContent = !player.isShowingAd();

        final String ooyalaNotification = OoyalaNotification.getNameOrUnknown(data);

        switch (ooyalaNotification) {
            case OoyalaPlayer.CURRENT_ITEM_CHANGED_NOTIFICATION_NAME:
                if (isShowingContent) {
                    // Close view if any
                    fireStop();
                    // Start
                    fireStart();
                }

                break;
            case OoyalaPlayer.TIME_CHANGED_NOTIFICATION_NAME:
                if (isShowingContent) {
                    if (!getFlags().isStarted()) fireStart();
                    if (!getFlags().isJoined()) fireJoin();
                    //YBLog.debug("playhead: " + player.getPlayheadTime());
                }

                break;
            case OoyalaPlayer.STATE_CHANGED_NOTIFICATION_NAME:
                if (isShowingContent) processPlayerState();
                break;
            case OoyalaPlayer.BUFFER_CHANGED_NOTIFICATION_NAME:
                //YBLog.debug("buffer: " + player.getBufferPercentage());
                break;
            case OoyalaPlayer.SEEK_COMPLETED_NOTIFICATION_NAME:
                //seekedHandler();
                if (!getFlags().isBuffering()) fireSeekBegin();

                getChronos().getSeek().setOffset(1L);
                fireSeekEnd();
                break;
            case OoyalaPlayer.PLAY_STARTED_NOTIFICATION_NAME:
                if (isShowingContent) fireJoin();
                break;
            case OoyalaPlayer.PLAY_COMPLETED_NOTIFICATION_NAME:
                if (isShowingContent) {
                    // Ignore ad end events
                    fireStop();
                }

                break;
            case OoyalaPlayer.ERROR_NOTIFICATION_NAME:
                if (isShowingContent) {
                    OoyalaException error = player.getError();

                    if (error != null) {
                        String errorCode = Integer.toString(error.getCode().ordinal());

                        if (lastErrorCode != null
                                && lastErrorCode.equals(errorCode)
                                && System.currentTimeMillis() - lastErrorSentTime < 5000) {
                            return;
                        }

                        lastErrorCode = errorCode;
                        lastErrorSentTime = System.currentTimeMillis();
                        fireError(error.getLocalizedMessage(), errorCode, null);
                    } else {
                        fireError("Unknown error", "9000", null);
                    }
                }

                fireStop();
                break;
            default:
                break;
        }
    }

    private void processPlayerState() {
        if (getPlayer() != null) {
            OoyalaPlayer.State state = getPlayer().getState();

            switch (state) {
                case INIT:
                    YouboraLog.debug("state: INIT");
                    break;
                case LOADING:
                    YouboraLog.debug("state: LOADING");
                    //if(!getFlags().isSeeking()){
                    fireBufferBegin();
                    //}
                    fireStart();
                    break;
                case READY:
                    YouboraLog.debug("state: READY");
                    break;
                case PLAYING:
                    YouboraLog.debug("state: PLAYING");
                    //if(!getFlags().isSeeking()){
                    fireBufferEnd();
                    //}
                    fireResume();
                    fireJoin();
                    break;
                case PAUSED:
                    YouboraLog.debug("state: PAUSED");
                    firePause();
                    break;
                case COMPLETED:
                    YouboraLog.debug("state: COMPLETED");
                    break;
                case SUSPENDED:
                    YouboraLog.debug("state: SUSPENDED");
                    break;
                case ERROR:
                    YouboraLog.debug("state: ERROR");
                    break;
            }
        }
    }

    // Get methods
    @Override
    public String getVersion() { return BuildConfig.VERSION_NAME + "-" + getPlayerName(); }

    @Override
    public String getPlayerName() { return "Ooyala"; }

    @Override
    public String getPlayerVersion() {
        return getPlayerName() + " " + OoyalaPlayer.getVersion();
    }

    @Override
    public Double getPlayhead() {
        try {
            if (getPlayer() != null) {
                OoyalaPlayer player = getPlayer();

                if (!player.isAdPlaying()) {
                    double playhead = player.getPlayheadTime();

                    if (playhead > 0) { // If playhead is 0, ignore it
                        lastPlayHead = playhead / 1000.0; // msec -> sec
                    }
                }
            }
        } catch (Exception ex) {
            YouboraLog.error(ex);
        }
        //YBLog.debug("playhead: " + lastPlayHead);
        return lastPlayHead;
    }

    @Override
    public Boolean getIsLive() {
        Boolean isLive = super.getIsLive();

        if (isCurrentItemNotNull())
            isLive = getPlayer().getCurrentItem().isLive();

        return isLive;
    }

    @Override
    public String getTitle() {
        String title = super.getTitle();

        if (isCurrentItemNotNull())
            title = getPlayer().getCurrentItem().getTitle();

        return title;
    }

    @Override
    public Long getBitrate() {
        Long bitrate = super.getBitrate();

        if (isCurrentItemNotNull() && getPlayer().getCurrentItem().getStream() != null) {
            long combinedBitrate = getPlayer().getCurrentItem().getStream().getCombinedBitrate();

            if (combinedBitrate > 0) bitrate = combinedBitrate;
        }

        return bitrate;
    }

    @Override
    public Double getDuration() {
        Double duration = super.getDuration();

        if (getPlayer() != null) duration = getPlayer().getDuration() / 1000.0;

        return duration;
    }

    @Override
    public String getResource() {
        String resource = super.getResource();

        if (isCurrentItemNotNull() && getPlayer().getCurrentItem().getStream() != null) {
            URL url = getPlayer().getCurrentItem().getStream().decodedURL();

            if (url != null) resource = url.toString();
        }

        return resource;
    }

    @Override
    public void fireStop() {
        super.fireStop();
        lastPlayHead = 0.0;
    }

    private boolean isCurrentItemNotNull() {
        return getPlayer() != null && getPlayer().getCurrentItem() != null;
    }
}
