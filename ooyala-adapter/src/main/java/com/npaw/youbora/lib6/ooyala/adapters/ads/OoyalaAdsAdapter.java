package com.npaw.youbora.lib6.ooyala.adapters.ads;

import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.adapter.AdAdapter;
import com.npaw.youbora.lib6.ooyala.BuildConfig;

import com.ooyala.android.OoyalaException;
import com.ooyala.android.OoyalaNotification;
import com.ooyala.android.OoyalaPlayer;

import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by Enrique on 14/02/2018.
 */

public class OoyalaAdsAdapter extends AdAdapter<OoyalaPlayer> implements Observer {

    private AdPosition adPosition;

    public OoyalaAdsAdapter(OoyalaPlayer player) {
        super(player);
        adPosition = AdPosition.UNKNOWN;
        registerListeners();
    }

    @Override
    public void registerListeners() {
        super.registerListeners();
        if (getPlayer() != null) getPlayer().addObserver(this);
    }

    @Override
    public void unregisterListeners() {
        if (getPlayer() != null) getPlayer().deleteObserver(this);
        super.unregisterListeners();
    }

    @Override
    public void update(Observable observable, Object data) {
        OoyalaPlayer player = (OoyalaPlayer) observable;

        if (getPlayer() != player) return;

        final String ooyalaNotification = OoyalaNotification.getNameOrUnknown(data);

        if (ooyalaNotification != OoyalaPlayer.BUFFER_CHANGED_NOTIFICATION_NAME && ooyalaNotification != OoyalaPlayer.TIME_CHANGED_NOTIFICATION_NAME) {
            YouboraLog.debug("ooyalaNotification - adnalyzer: " + ooyalaNotification);
        }

        switch (ooyalaNotification){
            case OoyalaPlayer.AD_POD_STARTED_NOTIFICATION_NAME:
                fireAdBreakStart();
                break;
            case OoyalaPlayer.AD_STARTED_NOTIFICATION_NAME:
                //joinAdHandler();
                fireStart();
                fireJoin();
                break;
            case OoyalaPlayer.AD_COMPLETED_NOTIFICATION_NAME:
                fireStop(new HashMap<String, String>(){{
                    put("adPlayhead","-1");
                    put("adPosition",adPosition.toString());
                }});
                break;
            case OoyalaPlayer.AD_SKIPPED_NOTIFICATION_NAME:
                fireSkip();
                break;
            case OoyalaPlayer.STATE_CHANGED_NOTIFICATION_NAME:
                if (player.isAdPlaying()) processPlayerState();
                break;
            case OoyalaPlayer.AD_POD_COMPLETED_NOTIFICATION_NAME:
                fireAdBreakStop();
                break;
            default:
                break;
        }
    }

    private void processPlayerState() {
        if (getPlayer() != null) {
            OoyalaPlayer.State state = getPlayer().getState();

            switch (state) {
                case INIT:
                    YouboraLog.debug("state ad: INIT");
                    break;
                case LOADING:
                    YouboraLog.debug("state ad: LOADING");
                    if (getFlags().isPaused())
                        fireResume();
                    else
                        fireBufferBegin();
                    break;
                case READY:
                    YouboraLog.debug("state ad: READY");
                    break;
                case PLAYING:
                    YouboraLog.debug("state ad: PLAYING");
                    fireBufferEnd();
                    fireResume();
                    break;
                case PAUSED:
                    YouboraLog.debug("state ad: PAUSED");
                    firePause();
                    break;
                case COMPLETED:
                    YouboraLog.debug("state ad: COMPLETED");
                    break;
                case SUSPENDED:
                    firePause();
                    YouboraLog.debug("state ad: SUSPENDED");
                    break;
                case ERROR:
                    YouboraLog.debug("state ad: ERROR");

                    OoyalaException error = getPlayer().getError();
                    if (error != null)
                        fireError(error.getLocalizedMessage(), Integer.toString(error.getCode().ordinal()), null);
                    else
                        fireError("Unknown error", "9000", null);
                    break;
            }
        }
    }

    @Override
    public String getVersion() { return BuildConfig.VERSION_NAME + "-" + getPlayerName(); }

    @Override
    public String getPlayerName() { return "Ooyala"; }

    @Override
    public String getPlayerVersion() { return getPlayerName() + " " + OoyalaPlayer.getVersion(); }

    @Override
    public Double getPlayhead() {
        Double adPlayhead = super.getPlayhead();

        try {
            if (getPlayer() != null) {
                OoyalaPlayer player = getPlayer();

                if (player.isAdPlaying()) adPlayhead = player.getPlayheadTime() / 1000.0;
            }
        } catch (Exception ex) {
            YouboraLog.error(ex);
        }

        return adPlayhead;
    }

    @Override
    public AdPosition getPosition() {
        AdPosition adPosition = AdPosition.UNKNOWN;

        if (getPlayer() != null && getPlayer().isAdPlaying()) {
            switch (getPlayer().getPlayingType()) {
                case MainContent:
                    // should never get here
                    break;
                case PreRollAd:
                    adPosition = AdPosition.PRE;
                    break;
                case MidRollAd:
                    adPosition = AdPosition.MID;
                    break;
                case PostRollAd:
                    adPosition = AdPosition.POST;
                    break;
            }
        }

        this.adPosition = adPosition;
        return adPosition;
    }
}
